# Static Web

This index.html will display your Bucket contents in a directory listing type of way. The pseudo-directory feature is not yet implemented.

- Download your Bucket Contents xml file and name it bucket.xml

```
curl -o bucket.xml https://s3.embassy.ebi.ac.uk/bucketname
```

- Place your bucket.xml alongside the index.html file.
